$(function () {
	// chapter Map Control
	var $currentTarget = null;
	$(document).on('mousemove', function (e) {
		var $chapter = $(e.target).closest('.chapter-group');
		if ($chapter.length) {
			if (!$chapter.is($currentTarget)) {
				$currentTarget = $chapter;
				$chapter.appendTo('.chapters');
				$('.chapter-group').not($chapter).removeClass('hovered');
				setTimeout(function () {
					$chapter.addClass('hovered');
				}, 0);
			}
		} else {
			$currentTarget = null;
			$('.chapter-group').removeClass('hovered');
		}
	});

	var $svg = $('.chapters-map').find('svg');

	$svg.find('.chapter-group').attr('tabindex', -1);

	$svg.attr('tabindex', 0);
	// endchapter

	$(document).on('click', '.chapter-group', function () {
		$('.chapters-select-chapter').selectable('select', this.id);
	});

	$('.chapters-results-item').each(function () {
		var county = $(this).data('county');

		if (county) {
			$(this).find('.chapters-results-icon svg .county-' + county).addClass('active');
		}
	});

	var $list = $('.chapters-results-list');

	$list.isotope({
		layout: 'fitRows',
		filter: function () {
			return true;
		}
	});

	var chapterFilters = [];

	function setFilters(value) {
		chapterFilters = value.split(',').filter(function (f) {
			return f !== ''; // Because JS return [""] when splitting an empty string
		});

		var filterString = '';

		if (chapterFilters.length) {
			filterString = chapterFilters.map(function (f) {
				return '[data-chapter="' + f + '"]';
			}).join();
		}

		$('.chapters-results').addClass('visible');
		$list.isotope({ filter: filterString });
		var count = $list.isotope('getFilteredItemElements').length;
		$('.chapters-results-nomatch').toggle(!count);

		// This sets the target to the top of the Chapters filter bar but not beyond the footer (the dropdowns get pretty long)
		var $footer = $('footer');
		var footerBottom = $footer.offset().top + $footer.outerHeight();
		var scrollTarget = Math.min($('.chapters-select').offset().top, footerBottom - window.innerHeight);

		window.scrollTo({
			top: scrollTarget,
			behavior: 'smooth'
		});
	}

	var $selectedDisplay = $('.chapters-results-chapter');
	$('.chapters-select-chapter').on('selector:change', function (e, value) {
		$('.chapters-select-county').selectable('reset');
		if (value) {
			var selectedElem = $(this).data('active');
			var selectedText = $(selectedElem).text();
			$selectedDisplay.text(selectedText + ' Chapter');
			$selectedDisplay.closest('h3').show();
		} else {
			$selectedDisplay.closest('h3').hide();
		}
		setFilters(value);
	});

	$('.chapters-select-county').on('selector:change', function (e, value) {
		$('.chapters-select-chapter').selectable('reset');
		if (value) {
			var selectedElem = $(this).data('active');
			var selectedText = $(selectedElem).text();
			$selectedDisplay.text(selectedText + ' County');
			$selectedDisplay.closest('h3').show();
		} else {
			$selectedDisplay.closest('h3').hide();
		}

		setFilters(value);
	});
});
