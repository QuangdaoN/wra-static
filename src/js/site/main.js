var isInEditMode = $('body').hasClass('sfPageEditor');

//region Initiators
// Stuff to init plugins go here

$(window).on('load', function () {
	lazyLoader = new LazyLoad();
});

$('.alert-ticker').ticker({
	item: '.alert-item',
	speed: 20,
	pauseOnHover: true
});

$('.slider-body').each(function () {
	if (!isInEditMode) {
		var sliderNav = $(this).siblings('.slider-nav');
		var sliderDots = $(this).siblings('.slider-dots');
		if ($(this).find('.copy-left')) {
			$(this).parent().addClass('ci-reverse');
		}

		$(this).slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			infinite: true,
			swipeToSlide: true,
			adaptiveHeight: true,
			appendArrows: sliderNav
		});
	} else {
		$(this).removeClass('slider-body');
		$(this).closest('.slider-wrap').removeClass('slider-wrap');
	}

});

$('.columns-slider').each(function () {
	var sliderNav = $(this).siblings('.columns-slider-nav');
	$(this).slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		infinite: false,
		swipeToSlide: true,
		mobileFirst: true,
		dots: true,
		appendDots: sliderNav,
		appendArrows: sliderNav,
		responsive: [
			{
				breakpoint: 767,
				settings: 'unslick'
			}
		]
	});
});

if ($('.target-slider').length) {
	$('.target-slider').slick({
		slidesToShow: 3,
		slidesToScroll: 1,
		infinite: false,
		swipeToSlide: false,
		responsive: [
			{
				breakpoint: 767,
				settings: 'unslick'
			}
		]
	});

	$('.target-select').on('selector:init selector:change', function (e, value) {
		var $slider = $(this).siblings('.target-slider');
		var $slides = $slider.find('.quick-link');

		$slider.slick('slickUnfilter');
		$slider.slick('slickFilter', function () {
			return $(this).data('target') === value;
		});

		$slides.removeClass('mobile-hide');

		$slides.filter(function () {
			return $(this).data('target') !== value;
		}).addClass('mobile-hide');

		$slider.slick('slickGoTo', 0);
	});
}

$(document).on('click', '.accordion__header', function () {
	var $content = $(this).siblings('.accordion__content');
	$(this).closest('.accordion__item').toggleClass('open-accordion');
	$content.slideToggle(200);
});

$('.image-gallery').each(function () {
	var $gallery = $(this);
	var $slider = $gallery.find('.image-gallery-slider');
	var $nav = $gallery.find('.image-gallery-nav');

	$gallery.removeClass('image-gallery-static');

	$slider.slick({
		infinite: true,
		slidesToShow: 1,
		centerMode: true,
		variableWidth: true,
		swipeToSlide: true,
		asNavFor: $nav,
		responsive: [
			{
				breakpoint: 1024,
				settings: {
					centerMode: false,
					variableWidth: false,
					adaptiveHeight: true
				}
			}
		]
	});

	$nav.slick({
		infinite: true,
		slidesToShow: 1,
		centerMode: true,
		variableWidth: true,
		swipeToSlide: true,
		asNavFor: $slider,
		focusOnSelect: true,
		arrows: false,
		responsive: [
			{
				breakpoint: 1024,
				settings: 'unslick'
			}
		]
	});
});

$(document).on('afterChange', function () {
	lazyLoader.update();
});
//endregion

//region Input
// Handles input states as class names on the parent to assist with styling when needed
$('.form-field input, .form-field textarea').each(function () {
	$(this).closest('.form-field').toggleClass('not-empty', $(this).val() !== '');
	$(this).on('focus', function () {
		$(this).closest('.form-field').addClass('focused');
	}).on('blur', function () {
		$(this).closest('.form-field').removeClass('focused');
	}).on('keydown keyup change load', function () {
		$(this).closest('.form-field').toggleClass('not-empty', $(this).val() !== '');
	});
});

$('.form-field .button-dropdown').on('selector:init selector:change', function (e, value) {
	$(this).closest('.form-field').toggleClass('not-empty', value !== '');
}).on('focus', function () {
	$(this).closest('.form-field').addClass('focused');
}).on('blur', function () {
	$(this).closest('.form-field').removeClass('focused');
});
//endregion

$('.ql-tab').on('click', function (e) {
	e.preventDefault();

	if (!$(this).hasClass('active')) {
		$('.ql-tab').removeClass('active');
		$(this).addClass('active');

		var target = $(this).data('for');

		$('.ql-dropdown-content').removeClass('active');
		$('#' + target).addClass('active');
	}
});

$('.ql-toggle').on('click', function (e) {
	e.preventDefault();
	$(this).parent().toggleClass('open');
});

//region Normalize - Sets default behaviors and apply ARIA/ADA-related attributes
$('a[href^="http"]').attr('target', '_blank');
$('a.button[href="#"]:not([role])').attr('role', 'button');

// role="listbox" and role="option" for button selectors are added on init.
//endregion

//region Responsive Adjustments
$(window).on('load resize', function (e) {
	var $util = $('.util');
	if ($(window).width() >= 1020) {
		$util.prependTo('#header');
	} else {
		$util.appendTo('.nav');
	}
});
//endregion

// Search Ghost (Element to make content wrap around search box)
$(function () {
	var $searchBox = $('.ql-dropdown');
	if ($searchBox.length) {
		var $searchGhost = $('#search-ghost');

		if (!$searchGhost.length) {
			$searchGhost = $('<div id="search-ghost">');
			$('#main').prepend($searchGhost);
		}

		var height = $searchBox.height() + 15;
		var offsetElements = ['.breadcrumbs-wrap', '.alert'];

		offsetElements.forEach(function (elem) {
			var $elem = $(elem);

			if ($elem.length) {
				height -= $elem.height();
			}
		});

		$searchGhost.height(height);
	}
});

//Readmore functionality on mobile
$(window).on('load', function () {
	var $currentElem = $('#main').children(':not(#search-ghost)').first();
	var $introElems = $('.intro-copy, .team-card');
	var introElements = [];

	while ($currentElem.is($introElems)) {
		introElements.push($currentElem[0]);
		$currentElem = $currentElem.next();
	}

	$(introElements).wrapAll('<div class="intro-content read-more-content" />');

	var $introWrapper = $('.intro-content');

	$introWrapper.wrap('<div class="intro-container read-more-wrapper" />');

	if ($(window).width() < 768) {
		$('.read-more-content').readmore({
			collapsedHeight: 250,
			embedCSS: false,
			moreLink: '<a href="#" class="button button-green button-arrow">Read more</a>',
			lessLink: ' ',
			afterToggle: function (trigger, element, expanded) {
				element.toggleClass('read-more-open', expanded);
			}
		});
	}
});

// This isn't crucial to the rendering so I'm loading asynchronously for perf reasons.
setTimeout(function () {
	if ($(window).width() >= 768) {
		$('.quick-link-wrap').each(function () {
			var count = 0;
			var colors = ['red', 'green', 'blue'];

			$(this).find('.quick-link').each(function () {
				var i = Math.floor(Math.random() * colors.length);

				var color = colors.splice(i, 1);

				$(this).addClass('color-' + color);

				count++;
				if (count >= 3) {
					count = 0;
					colors = ['red', 'green', 'blue'];
				}
			});
		});
	}
}, 0);
