$(function () {
	var categoryMap = {
		'6d1f28db-ab32-4a03-a8bb-8eb2fe660943': {
			id: '6d1f28db-ab32-4a03-a8bb-8eb2fe660943',
			name: 'News Release',
			icon: 'far fa-newspaper'
		},
		'b0743793-1ade-4d8e-8cf1-3551607d7311': {
			id: 'b0743793-1ade-4d8e-8cf1-3551607d7311',
			name: 'Alert',
			icon: 'fas fa-bell'
		},
		default: {
			name: 'News Item',
			icon: 'fas fa-question'
		}
	};

	$('.news-vue').each(function () {
		var $container = $(this);
		var loadCount = 25;
		var filter = $container.data('filter');

		var app = new Vue({
			el: $container[0],
			data: {
				items: [],
				loading: false,
				error: false,
				loaded: {
					years: [],
					categories: []
				},
				api: {
					next: 0,
					done: false
				},
				filters: {
					category: '*',
					year: '*'
				},
				buttonClass: []
			},
			computed: {
				sortedYears: function () {
					return this.loaded.years.sort(function (a, b) {
						return b - a;
					});
				}
			},
			methods: {
				loadItems: function () {
					var vm = this;
					vm.loading = true;
					var url = '/api/default/newsitems?$select=Category,Title,UrlName,PublicationDate&$orderby=PublicationDate desc&$top=' + loadCount + '&$skip=' + vm.api.next;

					if (filter) {
						var id = '';
						for (var cat in categoryMap) {
							if (categoryMap[cat].name === filter) {
								id = cat;
							}
						}
						url += '&$filter=Category/any(category: category eq ' + id + ')';
					}
					$.ajax(url)
						.done(function (response) {
							if (!response) {
								vm.loading = false;
								vm.api.done = true;
								return;
							}
							var items = response.value.map(function (item) {
								var cat = item.Category && item.Category[0];
								item.Date = moment(item.PublicationDate);
								var year = parseInt(item.Date.format('YYYY'));

								item.Year = year;

								if (vm.loaded.years.indexOf(year) < 0) vm.loaded.years.push(year);

								if (categoryMap[cat]) {
									item.Category = categoryMap[cat];
									if (vm.loaded.categories.indexOf(categoryMap[cat]) < 0) vm.loaded.categories.push(categoryMap[cat]);

								} else {
									item.Category = categoryMap['default'];
								}

								item.FullUrl = '/news-details' + item.Date.format('/YYYY/MM/DD') + '/' + item.UrlName;

								return item;
							});

							vm.items = vm.items.concat(items);
							if (items.length < loadCount) {
								vm.api.done = true;
							} else {
								vm.api.next += loadCount;
							}
							vm.loading = false;
						})
						.fail(function () {
							vm.loading = false;
							vm.api.done = true;
							vm.error = true;
							return;
						});

				},
				getItems: function (yr) {
					var vm = this;
					var items = this.items.filter(function (item) {
						return item.Year === yr && (vm.filters.category === '*' || vm.filters.category === item.Category.name);
					});
					return items;
				},
				filter: function (type, val) {
					this.filters[type] = val;
				},
				yearToColor: function (yr) {
					return ['red', 'green', 'blue'][yr % 3];
				},
				truncateString: function (str, maxLength) {
					maxLength = maxLength || 69; // maximum number of characters to extract

					var truncated = str;

					if (str.length > maxLength) {
						truncated = str.substr(0, maxLength - 3);
						truncated = truncated.substr(0, Math.min(truncated.length, truncated.lastIndexOf(' ')));
						truncated += '...';
					}

					//trim the string to the maximum length
					return truncated;
				}
			},
			created: function () {
				$container.show();
				this.loadItems();
			},
			mounted: function () {
				this.buttonClass = ['button-select', 'button-dropdown'];
			},
			updated: function () {
				var $categorySelector = $(this.$refs.selectCategory);
				var $yearSelector = $(this.$refs.selectYear);
				var vm = this;

				if (!$categorySelector.data('selector:initiated')) {
					$categorySelector.selectable();
					$categorySelector.dropdown();

					$categorySelector.on('selector:change', function (e, value) {
						vm.filter('category', value);
					});
				}

				if (!$yearSelector.data('selector:initiated')) {
					$yearSelector.selectable();
					$yearSelector.dropdown();

					$yearSelector.on('selector:change', function (e, value) {
						vm.filter('year', value);
					});
				}
			}
		});
	});
});
