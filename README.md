# Tosa Demo

## Uses NPM + Gulp + Nunjucks

To begin using this template

* Clone the repo: `git clone git@bitbucket.org:marcuswebteam/sales-tablet-website.git`

* Install dev depends inside the root folder: `npm install`

* Run dev: `gulp` This will start a dev server for you at `http://localhost:3000`

* The `gulp` task will auto compile sass, nunjuck and js files

* Nunjucks templates are found in the `pages` & `templates` folders

* Build Prod: `gulp build` This will give you live view of your production build at `http://localhost:3000`

* Production files are in dist