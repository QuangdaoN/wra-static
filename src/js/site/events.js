$(function () {

	$('.calendar-wrap').each(function () {
		var widgetId = $(this).data('widgetId');
		var $calendar = $(this).find('.calendar');
		var $calendarFilter = $(this).find('.calendar-filter');
		var $modal = $(this).next('.gmodal-overlay').find('.calendar-modal');
		var modal = $modal.modal();
		var eventFilter = '*';
		var calendar;

		function setFilter(filter) {
			eventFilter = filter;
			if (calendar) {
				calendar.rerenderEvents();
			}
		}

		function setView(view) {
			var desktopSize = $(window).width() > 767;

			if (calendar) {
				calendar.changeView(getView());
			}

			var title = view.title;
			$('.calendar-header-title').text(title);

			if (desktopSize) {
				$calendarFilter.removeClass('button-select');
				$calendarFilter.find('.button').each(function () {
					$(this).css('background-color', $(this).data('color'));
					$(this).css('color', getContrastYIQ($(this).data('color')));
				});
				setFilter('*');

			} else {
				$calendarFilter.addClass('button-select');
				$calendarFilter.find('.button').css('background-color', '');
				setFilter($calendarFilter.data('value'));
			}

		}

		$(this).find('.calendar-header-prev').on('click', function () {
			calendar.prev();
			return false;
		});

		$(this).find('.calendar-header-next').on('click', function () {
			calendar.next();
			return false;
		});

		$modal.on('modal:close', function () {
			if (window.history.pushState) {
				window.history.pushState(null, null, '#');
			} else {
				window.location.hash = '';
			}
		});

		function getView() {
			var desktopSize = $(window).width() > 767;
			var viewName = desktopSize ? 'month' : 'listMonth';
			return viewName;
		}

		function openModal(event) {
			var date;
			if (event.allDay) {
				date = event.start.format('MMM DD, YYYY');
				var endDate = event.end.subtract(1, 'days');
				if (event.end.diff(event.start)) {
					date += ' - ' + endDate.format('MMM DD, YYYY');
				}
			} else {
				date = event.start.format('MMM DD, YYYY | h:mma');
				if (event.end.diff(event.start)) {
					date += event.end.format(' - h:mma');
				}
			}
			$modal.find('.calendar-modal-title').text(event.title);
			$modal.find('.calendar-modal-date').text(date);
			$modal.find('.calendar-modal-description').html(event.description);
			var $img = $modal.find('.calendar-modal-image');

			if (event.featuredImage) {
				$img.attr('src', event.featuredImage);
				$img.show();
			} else {
				$img.hide();
			}

			modal.open();
		}

		function mapEventData(evt) {
			var calInfo = evt.Parent || {};

			var eventEnd = evt.EventEnd;

			if (evt.IsAllDay) {
				eventEnd = moment(eventEnd).add(1, 'days'); // Sitefintiy returns last day; FullCalendar expects first day after.
			}

			console.log(evt.Title, evt.EventStart, evt.EventEnd);

			return {
				title: evt.Title,
				description: evt.Content,
				allDay: evt.IsAllDay,
				start: moment(evt.EventStart).utc(),
				end: moment(eventEnd).utc(),
				eventUrl: evt.UrlName,
				url: evt.TargetUrl || '#',
				backgroundColor: calInfo.Color,
				textColor: calInfo.Color ? getContrastYIQ(calInfo.Color) : '',
				featuredImage: evt.featuredImage ? evt.featuredImage.url : '',
				calendarId: calInfo.Id
			};
		}

		function initCalendar(calendarFilterString) {
			var hash = window.location.hash.substr(1);

			var eventUrl = hash.indexOf('/') === 0 ? hash.substr(1) : null;
			var eventData;

			if (eventUrl) {
				$.ajax({
					url: '/api/default/events?$filter=UrlName eq \'' + eventUrl + '\''
				}).done(function (res) {
					eventData = res.value[0];
				}).fail(function (e) {
				}).always(function () {
					renderCalendar(eventData);
				});
			} else {
				renderCalendar();
			}

			function renderCalendar(defaultEvent) {
				var defaultDate = moment();

				if (defaultEvent) {
					defaultDate = moment(defaultEvent.EventStart);
					openModal(mapEventData(defaultEvent));
				}

				$calendar.fullCalendar({
					defaultView: getView(),
					defaultDate: defaultDate,
					header: false,
					fixedWeekCount: false,
					eventLimit: 2,
					timeFormat: ' ',
					listDayFormat: false,
					listDayAltFormat: 'ddd MMM DD YYYY',
					noEventsMessage: 'No Events Match the Selected Criteria',
					googleCalendarApiKey: 'AIzaSyBikxGkqSpQSAX_yRUKwOmfW58Iusomx9g',
					height: 'auto',
					eventRender: function (e, elem) {
						if (elem.hasClass('fc-list-item')) {
							elem.css({
								backgroundColor: e.backgroundColor,
								color: e.textColor
							});
						}
						return eventFilter === '*' || e.calendarId === eventFilter;
					},
					eventSources: [
						{
							url: 'en.usa#holiday@group.v.calendar.google.com',
							className: 'event-holiday'
						},
						function (start, end, timezone, callback) {
							if (!widgetId || !calendarFilterString) return callback([]);

							var query =
								'?$expand=' +
								'Parent,' +
								'FeaturedImage' +
								'&$filter=' +
								'((EventStart ge ' + start.toISOString() + ' and ' +
								'EventEnd le ' + end.toISOString() + ') or ' +
								'RecurrenceExpression ne null)';

							if (calendarFilterString) query += ' and ' + calendarFilterString;

							$.ajax({
								url: '/api/default/events' + query
							}).then(function (res) {
								var events = res.value.map(function (evt) {
									var eventData = mapEventData(evt);

									// Parse and set recurrence data
									if (evt.RecurrenceExpression) {
										var rruleExp = evt.RecurrenceExpression.match(/RRULE:(.*?)\r\n?/)[1];
										var recurrence = rrulestr(rruleExp).options;

										if (recurrence.byweekday) {
											eventData.dow = recurrence.byweekday.map(function (dow) {
												return dow + 1 > 6 ? 0 : dow + 1;
											});
											eventData.ranges = [
												{
													start: eventData.start,
													end: eventData.end
												}
											];

											eventData.start = moment(eventData.start).format('HH:MM');
											eventData.end = moment(eventData.end).format('HH:MM');
										}

									}

									return eventData;
								});

								callback(events);
							});
						}
					],
					dayRender: function (day, cell) {
						var dateText = day.format('D') === '1' ? day.format('MMM DD') : day.format('DD');

						$(cell).html('<span>' + dateText + '</span>');
					},
					loading: setView,
					windowResize: setView,
					viewRender: setView,
					eventClick: function (event, e) {
						if (!event.url || event.url === '#') {
							e.preventDefault();
							window.location.hash = '/' + event.eventUrl;
							openModal(event);
						}
					}
				});

				calendar = $calendar.fullCalendar('getCalendar');
			}

		}

		if (!widgetId) {
			initCalendar();
		} else {
			$.ajax({
				url: '/web-interface/calendars',
				data: {
					Id: widgetId
				}
			}).then(function (calendars) {
				if (calendars) {
					calendars.forEach(function (cal) {
						var $btn = $('<a href="#" class="button button-red" />');
						$btn.text(cal.Title);
						$btn.attr('data-color', cal.Color);
						$btn.attr('data-value', cal.CalendarId);

						$calendarFilter.append($btn);

					});

					$calendarFilter.selectable();
					$calendarFilter.on('selector:change', function (e, value) {
						setFilter(value);
					});

					var calendarFilterString = calendars.map(function (cal) {
						return '(Parent/Id eq ' + cal.CalendarId + ')';
					}).join(' or ');

					initCalendar(calendarFilterString);
				} else {
					initCalendar();
				}

			});
		}
	});
});