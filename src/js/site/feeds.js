if ($('.feeds-body').length) {
	function initSlides() {
		$('.feed-body').each(function () {
			var $feedSlide;
			var $feed = $(this);

			function newSlide() {
				var $slide = $('<div class="feed-slide" />');

				if ($feed.find('.feed-slide').length) {
					$slide.insertAfter($feed.find('.feed-slide').last());
				} else {
					$feed.prepend($slide);
				}

				return $slide;
			}

			var $items = $feed.children('.feed-item');

			$items.each(function (i) {
				if (!$feedSlide || $feedSlide.height() >= 500) $feedSlide = newSlide();

				$feedSlide.append(this);
			});
		});

		$('.feeds-body').addClass('magicalized');

		$('.feed-body').each(function () { // Chaining .each() #LikeABoss (This basically loops again after all of the previous ones are complete)
			var navContainer = $(this).siblings('.feed-nav');

			//region CHROOOOOOOOOOOOOOOOOOOOOOOOOME
			// This breaks non-Chrome :'(
			//$(this).on('init', function() {
			//	var $slides = $(this).closest('.feeds-body').find('.feed-slide');
			//
			//	var heights = $slides.map(function() {
			//		return $(this).height()
			//	});
			//
			//	console.log(this, heights.toArray());
			//
			//	var maxHeight = Math.max.apply(null, heights.toArray());
			//	console.log(maxHeight);
			//
			//	$slides.height(maxHeight);
			//});
			//endregion

			$(this).slick({
				slidesToShow: 1,
				slidesToScroll: 1,
				arrows: false,
				dots: true,
				appendDots: navContainer
			});

		});
	}

	$('.feeds-select').on('selector:init selector:change', function (e, value) {
		$('.feed-wrap').removeClass('active');
		var $feed = $('[data-feed="' + value + '"]');
		$feed.addClass('active');

		$($feed).resize();
	});

	function truncate(str, n) {

		return str.substr(0, n - 1) + (str.length > n ? '&hellip;' : '');
	}

	var $feedSocial = $('[data-feed="social"] .feed-body');

	$.ajaxSetup({ cache: true });
	$.getScript('https://connect.facebook.net/en_US/sdk.js', function () {
		FB.init({
			appId: '2028588437385946',
			autoLogAppEvents: false,
			version: 'v2.12'
		});

		FB.api('https://graph.facebook.com/v2.12/129810877038861/posts', {
			access_token: '2028588437385946|agFmU6MICsV_jHy_tiBoxeUw-l4',
			fields: ['id', 'message', 'created_time', 'permalink_url', 'full_picture', 'likes.summary(true)', 'shares', 'actions'],
			limit: 5
		}, function (response) {
			var raw = response.data;
			var data = raw.map(function (post) {
				var newData = {
					id: post.id,
					link: post.permalink_url,
					picture: post.full_picture,
					likes: post.likes.summary,
					shares: post.shares ? post.shares.count : 0,
					date: moment(post.create_time).format('dddd MMM. D, YYYY')
				};

				var content = truncate(post.message, 150);
				newData.content = '<p>' + content.split(/\n+/).join('</p><p>') + '</p>';

				return newData;
			});

			Vue.filter('pluralize', function (value, str) {
				return value === 1 ? str : str + 's';
			});

			var app = new Vue({
				el: $feedSocial[0],
				data: { posts: data },
				methods: {
					share: function (url) {
						FB.ui(
							{
								method: 'share',
								href: url
							},
							// callback
							function (response) {
								//console.log(response);
							});
					},
					like: function (id) {
						alert(id);
					}
				}
			});

			initSlides();
		});
	});
}