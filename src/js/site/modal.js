$(function () {
	$.fn.modal = function (command) {
		var $modal = $(this);
		var $overlay = $modal.closest('.gmodal-overlay');
		var $focusable;
		var commands = {
			open: function () {
				$modal.data('modal:origin', document.activeElement);
				$modal.data('modal:open', true);
				$overlay.addClass('gmodal-open');
				$focusable = $modal.find(':focusable');

				$focusable.first().focus();
				$modal.trigger('modal:open');
			},
			close: function () {
				$overlay.removeClass('gmodal-open');
				$modal.data('modal:origin').focus();
				$modal.data('modal:origin').focus();
				$modal.data('modal:open', false);
				$modal.trigger('modal:close');
			},
			get: function () {
				return $modal;
			}
		};

		function isValidCommand(cmd) {
			for (c in commands) {
				if (commands[c] === cmd) return true;
			}
			return false;
		}

		if (isValidCommand(command)) {
			if ($modal.data('modal')) {
				return commands[command]();
			}
		} else {
			if (!$modal.data('modal')) {
				$modal.data('modal', true);

				$modal.on('click', '.gmodal-close', function (e) {
					e.preventDefault();
					commands.close();
				});
				$(document).on('keydown', function (e) {
					if ($modal.data('modal:open')) {
						if (e.which === 27) {
							commands.close();
						}

						if (e.which === 9) {
							e.preventDefault();

							var currentIndex = $focusable.index(document.activeElement);

							if (currentIndex === -1) {
								$focusable.first().focus();
							}

							var target = currentIndex;

							if (e.shiftKey) {
								target--;
								if (target < 0) target = $focusable.length - 1;
							} else {
								target++;
								if (target >= $focusable.length) target = 0;
							}

							$focusable.eq(target).focus();
						}
					}
				});
			}

			return commands;
		}

	};
});