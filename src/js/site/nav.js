$(document).ready(function () {

	/******* MOBILE Hamburger icon *******/
	var $navBtn = $('.nav-toggle');
	var $nav = $('#nav');
	var $navbar = $('.navbar');

	$navBtn.on('click', function (e) {
		e.preventDefault();

		$navbar.toggleClass('nav-open');
	});

	/***** Mobile Dropdown *****/
	//var openBttn = $('a.touchNav');
	//
	//openBttn.on('click', function (e) {
	//	e.preventDefault();
	//	$('li.nav__item--primary').removeClass('toggled');
	//	$(this).closest('li.nav__item--primary').addClass('toggled');
	//});

	/**** Making the Main Nav Dropdowns Accessible ****/

	$(function () {
		$('.nav-list.nav-list-l1').setup_navigation();
	});

	var keyCodeMap = {
		48: '0',
		49: '1',
		50: '2',
		51: '3',
		52: '4',
		53: '5',
		54: '6',
		55: '7',
		56: '8',
		57: '9',
		59: ';',
		65: 'a',
		66: 'b',
		67: 'c',
		68: 'd',
		69: 'e',
		70: 'f',
		71: 'g',
		72: 'h',
		73: 'i',
		74: 'j',
		75: 'k',
		76: 'l',
		77: 'm',
		78: 'n',
		79: 'o',
		80: 'p',
		81: 'q',
		82: 'r',
		83: 's',
		84: 't',
		85: 'u',
		86: 'v',
		87: 'w',
		88: 'x',
		89: 'y',
		90: 'z',
		96: '0',
		97: '1',
		98: '2',
		99: '3',
		100: '4',
		101: '5',
		102: '6',
		103: '7',
		104: '8',
		105: '9'
	};

	$.fn.setup_navigation = function (settings) {

		$nav = $(this);

		settings = jQuery.extend({
			menuHoverClass: 'show-menu'
		}, settings);
		// Add ARIA role to menubar and ProductMenu items
		$(this).attr('role', 'menubar').find('li').attr('role', 'menuitem');
		var $navLinks = $(this).find('.nav-link');

		// Added by Terrill: (removed temporarily: doesn't fix the JAWS problem after all)
		// Add tabindex="0" to all top-level links
		// Without at least one of these, JAWS doesn't read widget as a ProductMenu, despite all the other ARIA
		//$navLinks.attr('tabindex','0');

		// Set tabIndex to -1 so that $navLinks can't receive focus until ProductMenu is open
		$navLinks.siblings('ul')
			.attr({ 'aria-hidden': 'true', 'role': 'ProductMenu' })
			.find('a')
			.attr('tabIndex', -1);

		// Adding aria-haspopup for appropriate items
		$navLinks.each(function () {
			$(this).parent('.has-child').attr('aria-haspopup', 'true');
		});

		function toggleNav(itemElem, force) {
			var isOpen = $(itemElem).children('.' + settings.menuHoverClass).length > 0;
			if (!isOpen || force === 'open') {
				$(itemElem).addClass('open');
				$(itemElem).siblings('.nav-item')
					.removeClass('open')
					.attr('aria-hidden', 'false')
					.find('.' + settings.menuHoverClass)
					.attr('aria-hidden', 'true')
					.removeClass(settings.menuHoverClass)
					.find('a')
					.attr('tabIndex', -1);

				$(itemElem).children('ul')
					.attr('aria-hidden', 'false')
					.addClass(settings.menuHoverClass)
					.find('> li > a').attr('tabIndex', 0);
			} else if (isOpen || force === 'close') {
				$(itemElem).removeClass('open');
				$(itemElem).closest('ul')
					.attr('aria-hidden', 'false')
					.find('.' + settings.menuHoverClass)
					.attr('aria-hidden', 'true')
					.removeClass(settings.menuHoverClass)
					.find('a')
					.attr('tabIndex', -1);
			}
		}

		if ($(window).width() >= 1020) {
			$(this).find('.nav-item').on('mouseover', function () {
				toggleNav(this, 'open');
			}).on('mouseout', function () {
				toggleNav(this, 'close');
			});

			$navLinks.focus(function () {
				toggleNav($(this).parent(), 'open');
			});

			$(document).click(function () {
				$('.' + settings.menuHoverClass).attr('aria-hidden', 'true').removeClass(settings.menuHoverClass).find('a').attr('tabIndex', -1);
			});
		} else {
			$navLinks.filter(function () {
				return $(this).parent('.has-child').length > 0;
			}).on('click', function (e) {
				if(e.target.matches('.nav-child-indicator')) {
					e.preventDefault();
					toggleNav($(this).parent());
				}
			});
		}

		// Bind arrow keys for navigation
		$('.nav-link-l1').keydown(function (e) {
			if (e.keyCode === 37) { // Left
				e.preventDefault();
				// This is the first item
				if ($(this).parent('li').prev('li').length === 0) {
					$(this).closest('ul').find('> li').last().children('a').first().focus();
				} else {
					$(this).parent('li').prev('li').children('a').first().focus();
				}
			} else if (e.keyCode === 38) { // Up
				e.preventDefault();
				if ($(this).siblings('ul').length > 0) {
					$(this).siblings('ul')
						.attr('aria-hidden', 'false')
						.addClass(settings.menuHoverClass)
						.find('a').attr('tabIndex', 0)
						.last().focus();
				}
			} else if (e.keyCode === 39) { // Right
				e.preventDefault();
				// This is the last item
				if ($(this).parent('li').next('li').length === 0) {
					$(this).closest('ul').find('> li').first().children('a').first().focus();
				} else {
					$(this).parent('li').next('li').children('a').first().focus();
				}
			} else if (e.keyCode === 40) { // Down
				e.preventDefault();
				if ($(this).siblings('ul').length > 0) {
					$(this).siblings('ul')
						.attr('aria-hidden', 'false')
						.addClass(settings.menuHoverClass)
						.find('a').filter('[tabIndex=0]')
						.first().focus();
				}
			} else if (e.keyCode === 13 || e.keyCode === 32) {
				// If submenu is hidden, open it
				e.preventDefault();
				$(this).siblings('ul[aria-hidden=true]')
					.attr('aria-hidden', 'false')
					.addClass(settings.menuHoverClass)
					.find('a').filter('[tabIndex=0]')
					.first().focus();
			} else if (e.keyCode === 27) {
				e.preventDefault();
				$('.' + settings.menuHoverClass)
					.attr('aria-hidden', 'true')
					.removeClass(settings.menuHoverClass)
					.find('a')
					.attr('tabIndex', -1);
			} else {
				$(this).parent('li').find('ul[aria-hidden=false] a').each(function () {
					if ($(this).text().substring(0, 1).toLowerCase() === keyCodeMap[e.keyCode]) {
						$(this).focus();
						return false;
					}
				});
			}
		});

		$('.nav-link:not(.nav-link-l1)').keydown(function (e) {
			if (e.keyCode === 37) {
				e.preventDefault();

				$(this).closest('ul')
					.siblings('a')
					.focus();
			} else if (e.keyCode === 38) {
				e.preventDefault();
				// This is the first item
				if ($(this).parent('li').prev('li').length === 0) {
					$(this).closest('ul').find('> li').last().children('a').first().focus();
				} else {
					$(this).parent('li').prev('li').children('a').first().focus();
				}
			} else if (e.keyCode === 39) {
				e.preventDefault();
				if ($(this).siblings('ul').length > 0) {
					$(this).siblings('ul')
						.attr('aria-hidden', 'false')
						.addClass(settings.menuHoverClass)
						.find('a').filter('[tabIndex=0]')
						.first().focus();
				}
			} else if (e.keyCode === 40) {
				e.preventDefault();
				// This is the last item
				if ($(this).parent('li').next('li').length === 0) {
					$(this).closest('ul').find('> li').first().children('a').first().focus();
				} else {
					$(this).parent('li').next('li').children('a').first().focus();
				}
			} else if (e.keyCode === 27 || e.keyCode === 37) {
				e.preventDefault();
				$(this)
					.closest('ul')
					.siblings('a').focus()
					.closest('ul').find('.' + settings.menuHoverClass)
					.attr('aria-hidden', 'true')
					.removeClass(settings.menuHoverClass)
					.find('a')
					.attr('tabIndex', -1);
			} else if (e.keyCode === 32) {
				e.preventDefault();
				window.location = $(this).attr('href');
			} else {
				var found = false;
				$(this).parent('li').nextAll('li').children('a').each(function () {
					if ($(this).text().substring(0, 1).toLowerCase() === keyCodeMap[e.keyCode]) {
						$(this).focus();
						found = true;
						return false;
					}
				});

				if (!found) {
					$(this).parent('li').prevAll('li').children('a').each(function () {
						if ($(this).text().substring(0, 1).toLowerCase() === keyCodeMap[e.keyCode]) {
							$(this).focus();
							return false;
						}
					});
				}
			}
		});

		// Hide ProductMenu if click or focus occurs outside of navigation
		$(this).find('a').last().keydown(function (e) {
			if (e.keyCode === 9) {
				// If the user tabs out of the navigation hide all menus
				$('.' + settings.menuHoverClass)
					.attr('aria-hidden', 'true')
					.removeClass(settings.menuHoverClass)
					.find('a')
					.attr('tabIndex', -1);
			}
		});

		$('.nav-list').each(function () {
			var $navList = $(this);
			var $navParent = $(this).closest('.nav-item');

			if ($navList.offset().left + $navList.width() > $(window).width()) {
				$navList.addClass('float-left');
				$navParent.addClass('child-left');
			}
		});

		/*
		$(this).click(function (e) {
			e.stopPropagation();
		});*/
	};

});