const gulp = require('gulp'),
	sass = require('gulp-sass'),
	prefix = require('gulp-autoprefixer'),
	del = require('del'),
	nunjucksRender = require('gulp-nunjucks-render'),
	browserSync = require('browser-sync'),
	imagemin = require('gulp-imagemin'),
	rename = require('gulp-rename'),
	cache = require('gulp-cache'),
	concat = require('gulp-concat'),
	uglify = require('gulp-uglify'),
	fs = require('fs'),
	sassVars = require('gulp-sass-vars'),
	runSequence = require('run-sequence');
const path = require('path');
const sourcemaps = require('gulp-sourcemaps');
const loremIpsum = require('lorem-ipsum');
const axios = require('axios');

const sfPath = 'C:/Dev/wisconsin-restaurant-assoc/ResourcePackages/Graydient/assets/';

//region Compile

gulp.task('compile:sass', function () {
	const vars = {
		assetsPath: ''
	};

	gulp.src('src/scss/**/*.scss')
		.pipe(sassVars(vars))
		.pipe(sass({
			outputStyle: 'expanded'
		}).on('error', sass.logError))
		.pipe(prefix())
		.pipe(gulp.dest('dist/css'))
		.pipe(browserSync.reload({
			stream: true
		}));

	gulp.src('src/scss/**/*.scss')
		.pipe(sassVars(vars))
		.pipe(sass({
			outputStyle: 'compressed'
		}).on('error', sass.logError))
		.pipe(prefix())
		.pipe(rename({
			suffix: '.min'
		}))
		.pipe(gulp.dest('dist/css'));
});

gulp.task('compile:nunjucks', function (done) {
	// Gets .html and .nunjucks files in pages
	return gulp.src([
			'src/views/pages/**/[^_]*.+(nunjucks)'
		])
		// Renders template with nunjucks
		.pipe(nunjucksRender({
			data: {
				nav: require('./data/nav'),
				ipsum(arg1, arg2) {
					const count = arg2 ? Math.floor(Math.random() * (arg2 - arg1 + 1)) + arg1 : arg1;

					return loremIpsum({
						count,
						units: 'words'
					});
				},
				clean(str) {
					return str.replace(/[\W]+/, '-').toLowerCase();
				}
			},
			path: ['src/views/templates', 'src/views/partials', 'src/images']
		}).on('error', error => {
			console.log(error);
			done();
		}))
		// output files in app folder
		.pipe(gulp.dest('dist'));
});

gulp.task('compile:scripts', function (done) {
	fs.readFile('./js-bundles.json', 'utf-8', (err, data) => {
		if (err) return console.log(err);

		const bundles = JSON.parse(data);
		const keys = Object.keys(bundles);

		let completeCount = 0;

		function cont() {
			completeCount++;
			if(completeCount >= keys.length) done();
		}

		keys.forEach(name => {
			const bundle = bundles[name];

			gulp.src(bundle)
				.pipe(sourcemaps.init())
				.pipe(concat(`${name}.js`))
				.pipe(sourcemaps.write())
				.pipe(gulp.dest('dist/js'));

			gulp.src(bundle)
				.pipe(sourcemaps.init())
				.pipe(uglify().on('error', function (e) {
					const err = e.cause;
					console.error(`${err.message} in ${err.filename} | Line: ${err.line} | Col: ${err.col}`);
					this.emit('end');
				}))
				.pipe(concat(`${name}.min.js`))
				.pipe(sourcemaps.write())
				.pipe(gulp.dest('dist/js'));

			cont();
		});
	});
});

gulp.task('compile:fonts', function () {
	return gulp.src('src/fonts/**/*.*')
		.pipe(gulp.dest('dist/fonts'));
});

gulp.task('compile:images', function () {
	return gulp.src('src/images/**/*.*')
		// Caching images that ran through imagemin
		//.pipe(cache(imagemin({
		//	interlaced: true
		//})))
		.pipe(gulp.dest('dist/images'));
});
gulp.task('compile', ['compile:sass', 'compile:nunjucks', 'compile:scripts', 'compile:images', 'compile:fonts']);
//endregion

//region Stream
// Tasks to migrate all the assets to the SF project.
gulp.task('stream:sass', function () {
	return del([sfPath + 'src/project/sass/**', '!' + sfPath + 'src/project/sass'], {
		force: true
	}).then(function () {
		return gulp.src('src/scss/**/*.scss')
			.pipe(gulp.dest(sfPath + 'src/project/sass'));
	});
});

gulp.task('stream:js-dev', function () {
	return del([sfPath + 'src/js/**', '!' + sfPath + 'src/js'], {
		force: true
	}).then(function () {
		return gulp.src('src/js/**/*.js')
			.pipe(gulp.dest(sfPath + 'src/js'));
	});
});

gulp.task('stream:js', function () {
	return del([sfPath + 'dist/js/**', '!' + sfPath + 'dist/js'], {
		force: true
	}).then(function () {
		return gulp.src('dist/js/**/*.js')
			.pipe(gulp.dest(sfPath + 'dist/js'));
	});
});

gulp.task('stream:css', function () {
	return gulp.src('dist/css/**/*.css')
		.pipe(gulp.dest(sfPath + 'dist/css'));
});

gulp.task('stream:images', function () {
	return gulp.src('dist/images/**/*.*')
		.pipe(gulp.dest(sfPath + 'dist/images'));
});

gulp.task('stream:fonts', function () {
	return gulp.src('dist/fonts/**/*.*')
		.pipe(gulp.dest(sfPath + 'dist/fonts'));
});

gulp.task('stream:bundle', function (done) {
	fs.readFile('./js-bundles.json', 'utf-8', (err, data) => {
		if (err) return console.log(err);

		const bundles = JSON.parse(data);
		const newBundles = {};

		Object.keys(bundles).forEach(name => {
			const newBundle = bundles[name].map(e => 'assets/' + e);

			newBundles[name] = newBundle;
		});

		fs.writeFile(path.resolve(sfPath, '../js-bundles.json'), JSON.stringify(newBundles, null, '\t'), function () {
			done();
		});
	});
});

gulp.task('stream', ['stream:sass', 'stream:js-dev', 'stream:bundle', 'stream:images', 'stream:fonts']);
//endregion

//region Dev

function proxyMiddleware(src,dest) {
	return {
		route: src,
		handle(req, res, next) {
			//axios.get('http://localhost:60876' + src + req.url)
			axios.get('http://wra-dev.graydientlabs.com' + src + req.url)
				.then(resp => {
					res.setHeader('Content-Type', 'application/json');
					res.end(JSON.stringify(resp.data));
					next();
				})
				.catch(err => {
					next(err);
				});
		}
	}
}

// Gulp BrowserSync
gulp.task('browserSync', function () {
	browserSync.init({
		server: {
			baseDir: 'dist',
			index: 'index.html'
		},
		middleware: [
			proxyMiddleware('/web-interface'),
			proxyMiddleware('/api')
		],
		ghostMode: false,
		notify: false
	});
});

gulp.task('watch', ['browserSync'], function () {
	gulp.watch('src/scss/**/*.scss', ['compile:sass']);
	gulp.watch('src/views/**/*.+(nunjucks)', ['compile:nunjucks']);
	gulp.watch(['src/js/**/*.js', './js-bundles.json'], ['compile:scripts']);
	gulp.watch('src/images/**/*.*', ['compile:images']);
	gulp.watch('src/fonts/**/*.*', ['compile:fonts']);
	gulp.watch('dist/js/**/*.js', browserSync.reload);
	gulp.watch('dist/**/*.html', browserSync.reload);
});
//endregion

gulp.task('default', ['compile', 'watch']);

// Gulp Delete
gulp.task('clean:dist', function () {
	return del.sync('dist');
});

// gulp clear cache
gulp.task('cache:clear', function (callback) {
	return cache.clearAll(callback);
});