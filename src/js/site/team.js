$(function () {
	$('.team-member').each(function () {
		var $img = $(this).find('.team-member-photo');

		var images = {
			idle: $img.attr('data-src') || $img.attr('src'),
			hover: $img.attr('data-hover-src')
		};

		//console.log(images);

		if (images.hover) {
			$(this).on('mouseenter', function () {
				$img.attr('src', images.hover);
			}).on('mouseleave', function () {
				$img.attr('src', images.idle);
			});
		}
	});

});

// Image Preloader - Load hover images in the background to reduce the delay

// https://stackoverflow.com/a/14438954/6425704
function onlyUnique(value, index, self) {
	return self.indexOf(value) === index;
}

$(window).on('load', function () {
	var hoverImages = $('.team-member .team-member-photo')
		.map(function () {
			return $(this).data('hoverSrc');
		})
		.toArray()
		.filter(onlyUnique);

	var $img = $('<img />');

	$img.css({
		position: 'fixed',
		left: -9999,
		top: -9999,
		maxWidth: 250
	});

	$img.appendTo('body');

	(function preloadImage(i) {
		$img.attr('src', hoverImages[i]);

		$img.one('load', function () {
			if (i + 1 < hoverImages.length) {
				preloadImage(i + 1);
			} else {
				$img.remove();
			}
		});
	})(0);
});
