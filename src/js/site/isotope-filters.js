$(window).on('load', function () {
	$filterables = $('[data-filter-for]');

	$filterables.each(function () {
		var target = $(this).data('filterFor');
		var filterAttribute = $(this).data('filterBy');

		var $list = $(target);

		$list.isotope({
			layout: 'fitRows',
			percentPosition: true
		});

		$list.imagesLoaded().progress(function () {
			$list.isotope('layout');
		});

		$(this).on('selector:init selector:change', function (e, value) {
			$list.isotope({ filter: value === '*' ? '' : '[' + filterAttribute + '*="' + value + '"]' });
		});
	});
});