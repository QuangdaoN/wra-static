//// Initiate functionality for button selectors
//
(function ($) {
	$.fn.dropdown = function () {
		return this.each(function () {
			if ($(this).data('dropdown:initiated')) return true;
			$(this).data('dropdown:initiated', true);

			var $dropdownContainer = $(this);
			var $dropdownWrap = $(this).find('.button-dropdown-wrap');
			var $dropdownToggle = $(this).find('.button-dropdown-toggle');
			$dropdownToggle.attr('tabindex', 0);

			var enabled = function () {
				//console.log($dropdownContainer.hasClass('button-dropdown'));
				return $dropdownContainer.hasClass('button-dropdown');
			};

			if ($dropdownContainer.data('dropdown:state') === undefined) {
				$dropdownContainer.data('dropdown:state', false);

				$dropdownContainer.on('dropdown:toggle', function (e, state) {
					var targetState = typeof state === 'undefined' ? !$dropdownContainer.data('dropdown:state') : state;

					e.preventDefault();
					//if ($dropdownWrap.find(':focus') && !targetState) $dropdownToggle.focus();
					$dropdownContainer.toggleClass('open', targetState);
					$dropdownContainer.data('dropdown:state', targetState);

					if (enabled()) {
						var ti = targetState ? 0 : -1;
						$dropdownWrap.find(':focusable').attr('tabindex', ti);
					}
				});

				$dropdownContainer.on('dropdown:close', function (e) {
					if (!$dropdownContainer.data('dropdown:state')) {
						return true;
					}
					e.preventDefault();
					$dropdownContainer.trigger('dropdown:toggle', false);
				});

				$dropdownContainer.on('dropdown:open', function (e) {
					if ($dropdownContainer.data('dropdown:state')) {
						return true;
					}
					e.preventDefault();
					$dropdownContainer.trigger('dropdown:toggle', true);
				});

				$dropdownContainer
					.on('click', function (e) {
						e.preventDefault();
					}) // Click triggers on mouse up, but mousedown triggers focus :/
					.on('mousedown touchstart', '.button-dropdown-toggle', function (e) {
						if (!enabled()) return true;
						e.preventDefault();
						var state = $dropdownContainer.data('dropdown:state');
						$(this).focus();
						// This instead of triggering dropdown:toggle automatically so we can listening on the open/close events if necessary
						if (state) {
							$dropdownContainer.trigger('dropdown:close');
						} else {
							$dropdownContainer.trigger('dropdown:open');
						}
					});

				$dropdownContainer.on('focus', '*', function () {
					if (enabled()) $dropdownContainer.trigger('dropdown:open');
				}).on('blur', '*', function (e) {
					//console.log(e.relatedTarget);
					if (enabled() && !$dropdownContainer.find(e.relatedTarget).length && $dropdownContainer.data('dropdown:state')) $dropdownContainer.trigger('dropdown:close');
				});

				$dropdownContainer.on('keydown', '.button-select-option', function (e) {
					if (!enabled()) return true;

					if (e.which === 27) {
						e.preventDefault();
						$dropdownToggle.focus();
						$dropdownContainer.trigger('dropdown:close');
					}

					if (e.which === 40) {
						e.preventDefault();
						$.tabNext();
					}

					if (e.which === 38) {
						e.preventDefault();
						$.tabPrev();
					}
				});

				$dropdownContainer.on('keydown', '.button-dropdown-toggle', function (e) {
					if (!enabled()) return true;

					var isOpen = $dropdownContainer.data('dropdown:state');
					if (e.which === 27) {
						e.preventDefault();
						$dropdownContainer.trigger('dropdown:close');
					}

					if (e.which === 13 || e.which === 32) {
						e.preventDefault();
						$dropdownContainer.trigger('dropdown:toggle');
					}

					if (e.which === 40) {
						if (isOpen) {
							e.preventDefault();
							$.tabNext();
						} else {
							e.preventDefault();
							$dropdownContainer.trigger('dropdown:open');
						}
					}

					if (e.which === 38) {
						if (isOpen) {
							e.preventDefault();
							$dropdownContainer.trigger('dropdown:close');
						}
					}
				});

				// This was a bad idea.
				//$(window).on('scroll', function () {
				//	$dropdownContainer.trigger('dropdown:close');
				//});
			}
		});
	};

//region Button Selectors
	// I wanted to call this .selector but older versions of jQuery uses that property already.
	$.fn.selectable = function (command, arg) {
		if (command) {
			if (command === 'select') {
				var option = $(this).find('.button-select-option').filter(function () {
					return $(this).data('value') === arg;
				});

				if (option.length) $(this).trigger('selector:select', { elem: option, silent: 'close' });
			}
			if (command === 'reset') {
				var elem = $(this).data('selector:default');
				$(this).trigger('selector:select', { elem: elem, silent: '*' });
			}
		} else {
			return this.each(function () {
				//region Setup
				if ($(this).data('selector:initiated')) return true;
				$(this).data('selector:initiated', true);

				var $selectList = $(this);
				var $wrapper = $selectList.find('.button-select-wrap');
				if (!$wrapper.length) {
					$wrapper = $('<div class="button-select-wrap" />');

					$selectList.find('.button').each(function () {
						$(this).addClass('button-select-option').attr('role', 'option').appendTo($wrapper);
					});

					$selectList.append($wrapper);
				}

				var $dropdown;

				$wrapper.attr('role', 'listbox');

				if ($selectList.is('.button-dropdown, .button-dropdown-mobile')) {
					//$selectList.addClass('button-dropdown');
					$wrapper.addClass('button-dropdown-wrap');

					if (!$selectList.data('dropdown:initiated')) {
						$dropdown = $('<a role="button" class="button button-blue button-dropdown-toggle" />');
						$dropdown.prependTo($selectList);
						$selectList.dropdown();

						if ($selectList.hasClass('button-dropdown-mobile')) {
							$(window).on('load resize', function () {
								var shouldBeDropdown = $(window).width() <= 768;

								$selectList.toggleClass('button-dropdown', shouldBeDropdown);
							});
						}
					}
				}
				//endregion

				//region Functionality
				$selectList.on('selector:select', function (e, options) {
					var elem;
					if (options.elem instanceof $) {
						elem = options.elem.get(0);
					} else if (typeof options.elem === 'number') {
						elem = $(this).find('.button-select-option').eq(options.elem);
					} else {
						elem = options.elem;
					}

					var silent = options.silent;

					function checkSilence(key) {
						return !silent || (silent !== key && silent !== '*');
					}

					var prevVal = $selectList.data('value');
					var value = $(elem).data('value');
					$(this).find('.selected').removeClass('selected').attr('aria-selected', '');
					$(elem).addClass('selected').attr('aria-selected', 'true');
					$selectList.data('active', elem);
					$selectList.data('value', value);

					if (prevVal !== value && checkSilence('change')) {
						$selectList.trigger('selector:change', value);
					}

					if ($(this).data('bind')) {
						$($(this).data('bind')).each(function () {
							var $bindTarget = $(this);

							if ($bindTarget[0].nodeName.toLowerCase() === 'input') {
								$bindTarget.val(value);
							} else {
								$bindTarget.text(value);
							}
						});
					}

					//console.log(elem);
					if ($dropdown) {
						var text = $(elem).data('selectedLabel') || $(elem).text();
						$dropdown.text(text);
						if (checkSilence('close')) $selectList.trigger('dropdown:close');
					}
				});

				$wrapper.on('click', '.button-select-option', function (e) {
					e.preventDefault();
					$wrapper.removeClass('visible');
					$selectList.trigger('selector:select', { elem: this });
				});

				//region Init
				if ($wrapper.find('.selected').length === 0) {
					$selectList.trigger('selector:select', { elem: $wrapper.find('.button').first(), silent: true });
				} else {
					$selectList.trigger('selector:select', { elem: $wrapper.find('.selected').first(), silent: true });
				}

				$selectList.data('selector:default', $wrapper.find('.selected'));

				setTimeout(function () {
					$selectList.trigger('selector:init', $selectList.data('value'));
				}, 0);
				//endregion
			});
		}

	};
	//endregion

	$('.button-select').selectable();
	$('.button-dropdown').dropdown();

	$('label').on('click', function (e) {
		var id = $(this).attr('for');
		var $elem = $(id);

		if ($elem.length && $elem.hasClass('button-dropdown')) {
			e.preventDefault();
			$elem.focus();
			$elem.trigger('dropdown:open');
		}
	});

})(jQuery);