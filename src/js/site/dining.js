$('.dining-card-tabs').on('selector:change', function (e, value) {
	var $cardContainer = $(this).siblings('.dining-card-container');
	var $cardBodies = $cardContainer.find('.dining-card-body');

	$cardBodies.each(function () {
		$(this).toggleClass('active', $(this).hasClass('dining-card-' + value));
	});
});

$('.dining-search-advance-toggle').on('click', function (e) {
	e.preventDefault();
	$('.dining-search-advance').slideToggle();
});

(function () {
	var $servsafe = $('.dining-card-servsafe');
	var $tooltip = $('.dining-tooltip');

	$servsafe.on('mouseover', function () {
		var x = $(this).offset().left + ($(this).width() / 2) - ($tooltip.width() / 2);
		var y = $(this).offset().top + $(this).height() - 5; // Arrow height + 5

		$tooltip.css({
			left: x,
			top: y
		}).addClass('hovered');
	}).on('mouseout', function() {
		$tooltip.removeClass('hovered');
	});
})();